package db;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.engine.SessionFactoryImplementor;
public class db {
	private static HibernateSessionFactory HibernateSessionFactory;
	public static Connection connect = null;
	public static ResultSet rs = null;
	public static Statement stmt = null;
	public  db() {

		
	    try {
	    	Session session = HibernateSessionFactory.getSession();
			ConnectionProvider cp =((SessionFactoryImplementor)session.getSessionFactory()).getConnectionProvider();  
		    connect =  cp.getConnection();
	    	
	    	
	     
	    }
	    catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	  }
	
	  
	  public ResultSet executeQuery(String sql) {
		  System.out.println(sql);
			try
			{
				if(connect==null || connect.isClosed())
				{
					Session session = HibernateSessionFactory.getSession();
					ConnectionProvider cp =((SessionFactoryImplementor)session.getSessionFactory()).getConnectionProvider();  
				    connect =  cp.getConnection();	
				}
				 
			    stmt = connect.createStatement();
				rs=stmt.executeQuery(sql);
			}catch(SQLException ex)
			{
				ex.printStackTrace();
			}
			return rs;
		}
	  
	  public void executeUpdate(String sql){
		  System.out.println(sql);
	    	stmt=null;
	    	rs=null;
	    	try
	    	{   
	    		if(connect==null || connect.isClosed()){
					Session session = HibernateSessionFactory.getSession();
					ConnectionProvider cp =((SessionFactoryImplementor)session.getSessionFactory()).getConnectionProvider();  
				    connect =  cp.getConnection();	
				}
	    		stmt = connect.createStatement();
	    		stmt.executeUpdate(sql);
	    		connect.commit();
	    		//stmt.close();
	    		//session.beginTransaction().commit();
	    		//session.close();
	    		//connect.close();
	    	}
	    	catch(SQLException ex){
	    		ex.printStackTrace();
	    	}
	    }
	  
	  public void closeConnect(){
		  try
	    	{   
	    		connect.close();
	    	}
	    	catch(SQLException ex){
	    		ex.printStackTrace();
	    	}
		  
	  }
}
