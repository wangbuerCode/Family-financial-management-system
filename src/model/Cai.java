package model;

public class Cai {
	
	private Integer id ;
	private String xiang ;
	private Integer money ;
	private String user ;
	private String times ;
	private Integer types ;
	private String mark ;
	private String cai ;
	
	private String name ;
	private Integer num ;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getXiang() {
		return xiang;
	}
	public void setXiang(String xiang) {
		this.xiang = xiang;
	}
	public Integer getMoney() {
		return money;
	}
	public void setMoney(Integer money) {
		this.money = money;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getTimes() {
		return times;
	}
	public void setTimes(String times) {
		this.times = times;
	}
	
	
	public Integer getTypes() {
		return types;
	}
	public void setTypes(Integer types) {
		this.types = types;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public String getCai() {
		return cai;
	}
	public void setCai(String cai) {
		this.cai = cai;
	}
	
	
	

}
